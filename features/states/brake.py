import pygame
import time
import threading


class Brake(threading.Thread):
    def __init__(self, sense, stop_event, cb=None):
        super(Brake, self).__init__()
        self.sense = sense
        self.stop_event = stop_event
        self.cb = cb

        self._matrix = [[150, 0, 0]]*64
        self._color = (255, 0, 0)
        self._light_duration = 0.3
        self._light_interval = 0.3
        self._sound = 'sounds/8d82b5_Car_Screech_Sound_Effect.mp3'
        self._iterations = 10

    def signal(self):
        self.sense.clear()

        if not pygame.mixer.music.get_busy() and not self.stop_event.is_set():
            try:
                pygame.mixer.music.load(self._sound)
                pygame.mixer.music.play()
            except:
                pass

        for _ in range(0, self._iterations):
            if self.stop_event.is_set():
                return

            self.sense.set_pixels(self._matrix)
            time.sleep(self._light_duration)
            self.sense.clear()
            time.sleep(self._light_interval)

        if self.cb is not None:
            self.cb()

    def run(self):
        self.signal()


if __name__ == "__main__":
    from sense_hat import SenseHat
    pygame.mixer.init()

    stopper = threading.Event()
    thread = Brake(SenseHat(), stopper)
    thread.start()
    thread.join()
