import pygame
import time
import threading


class TurnLeft(threading.Thread):
    def __init__(self, sense, stop_event, braking=False, iterations=4, cb=None):
        super(TurnLeft, self).__init__()
        self._sense = sense
        self._stop_event = stop_event
        self._sound = "sounds/turn_signal.wav"
        self._color = (255, 0, 0) if braking else (255, 140, 0)
        self._iterations = iterations
        self._cb = cb

    def signal(self):
        if not pygame.mixer.music.get_busy():
            try:
                pygame.mixer.music.load(self._sound)
                pygame.mixer.music.play()
            except:
                pass

        for _ in range(0, self._iterations):
            if self._stop_event.is_set():
                return

            if not pygame.mixer.music.get_busy():
                try:
                    pygame.mixer.music.load(self._sound)
                    pygame.mixer.music.play()
                except:
                    print("Failed to play sounds")

            for y in range(0, 12):
                y = 11 - y
                if y < 8 and y >= 0:
                    for x in range(0, 8):
                        self._sense.set_pixel(x, y, self._color)

                if y <= 8 and y >= 1:
                    for x in range(1, 7):
                        self._sense.set_pixel(x, y-1, self._color)

                if y < 10 and y >= 2:
                    for x in range(2, 6):
                        self._sense.set_pixel(x, y-2, self._color)

                if y < 11 and y >= 3:
                    for x in range(3, 5):
                        self._sense.set_pixel(x, y-3, self._color)

                time.sleep(0.1)
            self._sense.clear()

        if self._cb is not None:
            self._cb()

    def run(self):
        self.signal()


if __name__ == "__main__":
    from sense_hat import SenseHat
    pygame.mixer.init()

    stopper = threading.Event()
    thread = TurnLeft(SenseHat(), stopper, (255, 140, 0))
    thread.start()
    thread.join()
