import pygame
import time

from states.turn_left import TurnLeft
from states.turn_right import TurnRight
from states.brake import Brake
from states.accident import Accident
from threading import Event, Thread
from sense_hat import SenseHat


class Manager(object):
    def __init__(self, sense):
        self._sense = sense
        self._stop_blinking = Event()
        self._stop_braking = Event()
        self._stop_accident = Event()

        self._blinking_thread = Thread()
        self._braking_thread = Thread()
        self._accident_thread = Thread()

    def join_all(self):
        if self._blinking_thread.is_alive():
            self._blinking_thread.join()
        if self._braking_thread.is_alive():
            self._braking_thread.join()
        if self._accident_thread.is_alive():
            self._accident_thread.join()

    def stop_all(self):
        self._stop_blinking.set()
        self._stop_braking.set()
        self._stop_accident.set()

        time.sleep(0.5)

        self._stop_blinking.clear()
        self._stop_braking.clear()
        self._stop_accident.clear()

    def reset(self):
        print('reset')
        self.stop_all()
        self._sense.set_pixels([[0, 0, 0]]*64)

    def start_blinking_left(self, braking=False, cb=None):
        self.stop(self._stop_blinking)

        if self._blinking_thread.is_alive():
            self._blinking_thread.join()
        self._blinking_thread = TurnLeft(self._sense, self._stop_blinking, braking, cb=cb)
        self._blinking_thread.start()

    def start_blinking_right(self, braking=False, cb=None):
        self.stop(self._stop_blinking)

        if self._blinking_thread.is_alive():
            self._blinking_thread.join()
        self._blinking_thread = TurnRight(self._sense, self._stop_blinking, braking, cb=cb)
        self._blinking_thread.start()

    def stop_blinking(self):
        self.stop(self._stop_blinking)

    def start_braking(self, cb=None):
        self.stop(self._stop_braking)

        if self._braking_thread.is_alive():
            self._braking_thread.join()
        self._braking_thread = Brake(self._sense, self._stop_braking, cb=cb)
        self._braking_thread.start()

    def start_accident(self, cb=None):
        self.stop_all()

        if self._accident_thread.is_alive():
            self._accident_thread.join()
        self._accident_thread = Accident(self._sense, self._stop_accident, cb=cb)
        self._accident_thread.start()

    @staticmethod
    def stop(event):
        event.set()
        time.sleep(0.5)
        event.clear()


if __name__ == "__main__":
    pygame.mixer.init()
    manager = Manager(SenseHat())
    manager.turn_left(braking=True)
    manager.turn_right(braking=True)
    manager.join_all()
