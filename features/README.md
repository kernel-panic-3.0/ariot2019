# ARIoT 2019 - Light controller

## Getting started

Using these instructions you can set up a controller for the Raspberry Pi Sense Hat and deploy it to a Raspberry Pi.

### Prerequisites

#### Hardware
```
Raspberry Pi 3
Raspberry Pi Sense Hat
```

#### Software
```
SSH
```

## Installation
```
Install Raspian on the Raspberry Pi
SSH to the Raspberry Pi, or log on using a connected monitor and keyboard/mouse
Run apt install redis-server, python3-redis, python3-transitions, python3-websockets, python3-flask
```

## Deployment
```
python3 light_server.py &
python3 light_publisher.py &
```

## System description

This project is made up of the following components:

light_fsm.py is a finite state machine based on the Python [transitions](https://github.com/pytransitions/transitions) library.
The module implements the state machine shown below:

![alt text](https://gitlab.com/kernel-panic-3.0/ariot2019/raw/master/features/fsm.png "Finite state machine")
![alt text](https://gitlab.com/kernel-panic-3.0/ariot2019/raw/master/features/system.png "System overview")


light_manager.py manages the different blinking actions, which are implemented as Threads, and will start and stop these as required in order to allow actions to be interrupted on state changes.

light_publisher.py listens to state changes made by the finite state machine (published on a Redis channel), and pushes these to a WebSocket so that external consumers can subscribe to updates.

light_server.py is a Flask REST API which allows clients of the light controller to push state change requests on the following endpoints:
```
/turn_left
/turn_right
/start_braking
/disable_lights
/accident
```

# Usage
Install redis-server, python3-redis, python3-transitions, python3-websockets, python3-flask

Run light_server.py and optionally light_publisher.py