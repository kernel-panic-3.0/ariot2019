from flask import Flask
from sense_hat import SenseHat
from light_manager import Manager
from light_fsm import Light
from light_publisher import server
import asyncio
import pygame
import websockets

app = Flask(__name__)
sense = SenseHat()
manager = Manager(sense)
light = Light("Rear Light", manager)


@app.before_first_request
def setup():
    pygame.mixer.init()


@app.route('/current_state')
def index():
    return light.state, 200


@app.route('/turn_left')
def request_turn_left():
    light.turn_left()
    return '', 200


@app.route('/turn_right')
def request_turn_right():
    light.turn_right()
    return '', 200


@app.route('/start_braking')
def request_start_braking():
    light.brake()
    return '', 200


@app.route('/accident')
def request_accident():
    light.accident()
    return '', 200


@app.route('/disable_lights')
def request_disable_lights():
    light.disable_lights()
    return '', 200


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5071)
