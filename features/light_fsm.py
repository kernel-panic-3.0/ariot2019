import pygame
import time

from transitions import Machine
from light_manager import Manager
from sense_hat import SenseHat
from redis import Redis


class Light(object):
    states = ['off', 'blinking_left', 'braking_left', 'blinking_right', 'braking_right', 'braking', 'accident']

    def __init__(self, name, manager):
        self.name = name
        self.manager = manager
        self.machine = Machine(model=self, states=Light.states, initial='off', ignore_invalid_triggers=True)

        self.machine.add_transition(source='off', dest='blinking_left',  trigger='turn_left')
        self.machine.add_transition(source='off', dest='blinking_right', trigger='turn_right')
        self.machine.add_transition(source='off', dest='braking',        trigger='brake')

        self.machine.add_transition(source='braking', dest='braking_left',  trigger='turn_left')
        self.machine.add_transition(source='braking', dest='braking_right', trigger='turn_right')

        self.machine.add_transition(source='blinking_left', dest='braking_left',   trigger='brake')
        self.machine.add_transition(source='blinking_right', dest='braking_right', trigger='brake')

        self.machine.add_transition(source='braking_left', dest='braking',       trigger='blinking_complete')
        self.machine.add_transition(source='braking_left', dest='braking_right', trigger='turn_right')

        self.machine.add_transition(source='braking_right', dest='braking',      trigger='blinking_complete')
        self.machine.add_transition(source='braking_right', dest='braking_left', trigger='turn_left')

        self.machine.add_transition(source='*', dest='accident', trigger='accident')
        self.machine.add_transition(source='*', dest='off',      trigger='disable_lights')

        self.redis = Redis(host='localhost', port=6379, db=0)

        pygame.mixer.init()

    def _state_change(self):
        print('current state: %s' % self.state)
        self.redis.publish('state', self.state)

    def on_enter_off(self):
        self._state_change()
        self.manager.stop_all()

    def on_enter_blinking_left(self):
        self._state_change()
        self.manager.stop_all()
        self.manager.start_blinking_left(cb=self.to_off)

    def on_exit_blinking_left(self):
        self.manager.stop_all()

    def on_enter_blinking_right(self):
        self._state_change()
        self.manager.stop_all()
        self.manager.start_blinking_right(cb=self.to_off)

    def on_exit_blinking_right(self):
        print('on_exit_blinking_right')
        self.manager.stop_all()

    def on_enter_braking_left(self):
        self._state_change()
        self.manager.stop_all()
        self.manager.start_blinking_left(braking=True, cb=self.to_braking)

    def on_exit_braking_left(self):
        self.manager.stop_all()

    def on_enter_braking_right(self):
        self._state_change()
        self.manager.stop_all()
        self.manager.start_blinking_right(braking=True, cb=self.to_braking)

    def on_exit_braking_right(self):
        self.manager.stop_all()

    def on_enter_braking(self):
        self._state_change()
        self.manager.stop_all()
        self.manager.start_braking(cb=self.to_off)

    def on_exit_braking(self):
        pass

    def on_enter_accident(self):
        self._state_change()
        self.manager.stop_all()
        self.manager.start_accident(cb=self.to_off)

    def on_exit_accident(self):
        self.manager.stop_all()
        self.manager.reset()


if __name__ == "__main__":
    sns = SenseHat()
    mgr = Manager(sns)
    light = Light("Rear Light", mgr)

    light.turn_left()
    time.sleep(1)
    light.brake()
    time.sleep(3)
    light.turn_right()
    time.sleep(10)
    light.accident()
    time.sleep(5)
    light.disable_lights()

    mgr.join_all()
    print("done")
