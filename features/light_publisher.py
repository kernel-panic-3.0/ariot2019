import asyncio
import websockets
from redis import Redis

WS_PORT = 8775


async def server(websocket, path):
    print("Client connected")
    redis = Redis(host='localhost', port=6379, db=0)
    pubsub = redis.pubsub()
    pubsub.subscribe('state')
    try:
        for item in pubsub.listen():
            data = item['data']
            try:
                print('publishing')
                print(data)
                await websocket.send(data)
            except TypeError:
                # Data could not be published
                pass
    except websockets.exceptions.ConnectionClosed:
        print("Client disconnected")
        pubsub.unsubscribe('state')
        pubsub.close()


if __name__ == "__main__":
    loop = asyncio.get_event_loop()

    print('Initializing websockets server on port %d' % WS_PORT)
    ws_server = websockets.serve(server, '0.0.0.0', WS_PORT)

    loop.run_until_complete(ws_server)
    loop.run_forever()