from sense_hat import SenseHat
from math import sqrt
import time
import urllib.request

accident_acc_def = 2.0
break_acc_def = 1.0
refresh_interval = 0.5
break_axis = 'z'
light_server = 'http://localhost:5051/'


def breaking_event():
    print('braking_event')
    urllib.request.urlopen(light_server + 'start_braking')


def accident_event():
    print('accident_event')
    urllib.request.urlopen(light_server + 'accident')


def reset_event():
    print('done')
    urllib.request.urlopen(light_server + 'disable_lights')


def main():
    sense = SenseHat()
    in_accident = False
    in_braking = False

    while True:
        raw_acc = sense.get_accelerometer_raw()
        abs_acc = sqrt(raw_acc['x']**2 + raw_acc['y']**2 + raw_acc['z']**2)
        if abs_acc > accident_acc_def:
            if not in_accident:
                in_accident = True
                in_braking = False
                accident_event()
        elif abs(raw_acc[break_axis]) > break_acc_def:
            if not in_accident and not in_braking:
                in_braking = True
                breaking_event()
        else:
            in_braking = False
            in_accident = False
            reset_event()

        time.sleep(refresh_interval)


if __name__ == "__main__":
    main()
