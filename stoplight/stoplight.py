import asyncio
import json
import websockets
import urllib.request
import time

DANGEROUS_ITEMS = ['Stop sign']


async def detect():
    async with websockets.connect('ws://192.168.103.189:8765') as websocket:
        while True:
            detection = await websocket.recv()
            detection = json.loads(detection)

            if any(d['name'] in DANGEROUS_ITEMS for d in detection):
                print("stop!!!!")
                urllib.request.urlopen('http://192.168.103.76:5071/start_braking')
				time.sleep(10)
				urllib.request.urlopen('http://192.168.103.76:5071/disable_lights')


if __name__ == "__main__":
    asyncio.get_event_loop().run_until_complete(detect())
	