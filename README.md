# ARIoT2019

This project comprises a set of different microservices, enabling an intelligent bicycle helmet

[pistreaming](./pistreaming) is a fork of the [pistreaming](https://github.com/waveform80/pistreaming) library, which enables pushing a stream from a Raspberry Pi camera to a WebSocket.
The forked version additionally samples an image once per second, which can be analyzed by the detection module.
 
The [detection](./detection) module periodically fetches the image sampled by pistreaming, and sends it to the Google Vision API for analysis.
The results of the analysis, in form of a list of labelled objects detected in the image, are made available as a WebSocket stream.

[stoplight](./stoplight) is a microservice that listens to the data stream from the detection module, and issues an API call to enable the helmet's stop light in case a dangerous object is detected.