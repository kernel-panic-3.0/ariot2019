from datetime import datetime, timedelta
import time
import pytz
from elasticsearch import Elasticsearch
import elasticsearch.helpers
import random
import json
from math import *

es = Elasticsearch(
    hosts=[
        '34.76.127.170'
    ],
    port=9200
)
index_prefix = 'smarthjelm-test'

es.indices.put_template(
    name=index_prefix,
    body=json.dumps(
        {
            "index_patterns": [index_prefix + "-*"],
            "settings": {
                "number_of_shards": 1,
                "number_of_replicas": 0
            },
            "mappings": {
                "_doc": {
                    "properties": {
                        "timestamp": {
                            "type": "date"
                        },
                        "temperature": {
                            "type": "float"
                        },
                        "humidity": {
                            "type": "float"
                        }
                    }
                }
            }
        }
    )
)

date_now = datetime.now(pytz.timezone(time.localtime().tm_zone))
start_date = date_now - timedelta(days=14)
dt = timedelta(seconds=5)

def gen_sensor_data():
    date_current = start_date
    while date_current < date_now:
        sensor_data = {
            "_index": '{index_prefix}-{date}'.format(
                index_prefix=index_prefix,
                date=date_now.strftime('%Y-%m-%d')
            ),
            "_type": "_doc",
            'temperature': 37.5 + random.expovariate(1)*sin(date_current.hour*2*pi/24),
            'humidity': abs(30 + random.gauss(10, 5)*sin(date_current.hour*2*pi/24)),
            'timestamp': date_current.isoformat()
        }
        date_current = date_current + dt
        yield sensor_data


elasticsearch.helpers.bulk(es, gen_sensor_data())
