#!/usr/bin/env python
from flask import Flask, Response, stream_with_context
from threading import Thread, Event
from redis import Redis

import asyncio
import json
import os
import websockets
from google.protobuf.json_format import MessageToDict, MessageToJson
from google.cloud import vision

WS_PORT = 8765

os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "/home/pi/key.json"

app = Flask(__name__)
client = vision.ImageAnnotatorClient()


class DetectorThread(Thread):
    def __init__(self, event):
        super(DetectorThread, self).__init__()
        self.stopped = event
        self.redis = Redis(host='localhost', port=6379, db=0)

    def run(self):
        try:
            while not self.stopped.wait(0.5):
                result = Detector.localize_objects("/home/pi/capture.jpg")
                result = json.dumps(result)
                self.redis.publish('labels', result)
        except:
            pass


class Detector(object):
    @staticmethod
    def localize_objects(path):
        """Localize objects in the local image.

        Args:
        path: The path to the local file.
        """

        with open(path, 'rb') as image_file:
            content = image_file.read()

        image = vision.types.Image(content=content)

        try:
            response = client.object_localization(image=image)
            response = MessageToDict(response, preserving_proto_field_name=True)
            response = response['localized_object_annotations']
        except:
            response = dict()

        return response


def activate_detection():
    stop = Event()
    thread = DetectorThread(stop)

    print('Starting detection thread')
    thread.start()


async def server(websocket, path):
    print("Client connected")
    redis = Redis(host='localhost', port=6379, db=0)
    pubsub = redis.pubsub()
    pubsub.subscribe('labels')
    try:
        for item in pubsub.listen():
            data = item['data']
            try:
                await websocket.send(data)
            except TypeError:
                # Data could not be published
                pass
    except websockets.exceptions.ConnectionClosed:
        print("Client disconnected")
        pubsub.unsubscribe('labels')
        pubsub.close()


if __name__ == "__main__":
    activate_detection()
    loop = asyncio.get_event_loop()

    print('Initializing websockets server on port %d' % WS_PORT)
    ws_server = websockets.serve(server, 'localhost', WS_PORT)

    loop.run_until_complete(ws_server)
    loop.run_forever()
