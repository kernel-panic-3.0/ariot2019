from datetime import datetime
import time
import pytz
from flask import Flask, jsonify
import grovepi
from elasticsearch import Elasticsearch
from threading import Thread
import json
import math

# Connect the Grove Temperature & Humidity Sensor Pro to digital port D4
# This example uses the blue colored sensor.
# SIG,NC,VCC,GND
sensor = 4  # The Sensor goes on digital port 4.

# temp_humidity_sensor_type
# Grove Base Kit comes with the blue sensor.
blue = 0    # The Blue colored sensor.
white = 1   # The White colored sensor.

_HEAD_SENSOR_DATA = None

app = Flask(__name__)
app.secret_key = b'?2\xe4\x9de3\xef\xa8f\xc3G; $\xdeszh\xcas\x8d2\xf5\xdb'

es = Elasticsearch(
    hosts=[
        '34.76.127.170'
    ],
    port=9200
)
index_prefix = 'smarthjelm-grovepi'

es.indices.put_template(
    name=index_prefix,
    body=json.dumps(
        {
            "index_patterns": [index_prefix + "-*"],
            "settings": {
                "number_of_shards": 1,
                "number_of_replicas": 0
            },
            "mappings": {
                "_doc": {
                    "properties": {
                        "timestamp": {
                            "type": "date"
                        },
                        "temperature": {
                            "type": "float"
                        },
                        "humidity": {
                            "type": "float"
                        }
                    }
                }
            }
        }
    )
)


def set_grovepi_data():
    global _HEAD_SENSOR_DATA
    while True:
        try:
            date_now = datetime.now(pytz.timezone(time.localtime().tm_zone))
            [temp, humidity] = grovepi.dht(sensor, blue)
            if math.isnan(temp) is False and math.isnan(humidity) is False and temp != -1:
                _HEAD_SENSOR_DATA = {
                    'temperature': temp,
                    'humidity': humidity,
                    'timestamp': date_now.isoformat()
                }
                es.index(
                    index='{index_prefix}-{date}'.format(
                        index_prefix=index_prefix,
                        date=date_now.strftime('%Y-%m-%d')
                    ),
                    doc_type='_doc',
                    body=json.dumps(_HEAD_SENSOR_DATA)
                )
                time.sleep(1)
        except KeyboardInterrupt:
            break
        except ValueError:
            print('Could not read temperature')
        except IOError:
            print('Error')


# Create and start thread for retrieving GPS data
th_head_temp = Thread(target=set_grovepi_data)
th_head_temp.daemon = True
th_head_temp.start()


@app.route('/get_head_sensor_data', methods=['GET'])
def get_head_temp():
    return jsonify(_HEAD_SENSOR_DATA)


if __name__ == '__main__':
    app.run(
        host='0.0.0.0',
        port=5679,
        debug=True
    )
