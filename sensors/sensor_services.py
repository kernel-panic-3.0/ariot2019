from sense_hat import SenseHat
from gps3 import gps3
import requests
import json
from threading import Thread
from datetime import datetime
from flask import Flask, jsonify
from elasticsearch import Elasticsearch
import time
import pytz
from math import sqrt

debug = False
google_maps_api_key = 'AIzaSyDTUj-EDU2m19HQCTUtfAsDOGGATsIb5Yo'
openweathermap_api_key = '2bf0497439bda99ec6dd90e9c97719da'

gps_socket = gps3.GPSDSocket()
data_stream = gps3.DataStream()
gps_socket.connect()
gps_socket.watch()

sense = SenseHat()

app = Flask(__name__)
app.secret_key = b'?2\xe4\x9de3\xef\xa8f\xc3G; $\xdeszh\xcas\x8d2\xf5\xdb'

es = Elasticsearch(
    hosts=[
        '34.76.127.170'
    ],
    port=9200
)
index_prefix = 'smarthjelm-sensors'

es.indices.put_template(
    name=index_prefix,
    body=json.dumps(
        {
            "index_patterns": [index_prefix + "-*"],
            "settings": {
                "number_of_shards": 1,
                "number_of_replicas": 0
            },
            "mappings": {
                "_doc": {
                    "properties": {
                        "speed": {
                            "type": "float"
                        },
                        "alt": {
                            "type": "float"
                        },
                        "timestamp": {
                            "type": "date"
                        },
                        "location": {
                            "type": "geo_point"
                        },
                        "humidity": {
                            "type": "float"
                        },
                        "temperature": {
                            "type": "float"
                        },
                        "pressure": {
                            "type": "float"
                        },
                        "gps_valid": {
                            "type": "boolean"
                        },
                        "acceleration": {
                            "properties": {
                                "x": {
                                    "type": "float"
                                },
                                "y": {
                                    "type": "float"
                                },
                                "z": {
                                    "type": "float"
                                },
                                "abs": {
                                    "type": "float"
                                }
                            }
                        }
                    }
                }
            }
        }
    )
)

es.ingest.put_pipeline(
    id='convert_to_human_units',
    body=json.dumps(
        {
            "description": "Convert temperature and pressure to human units",
            "processors": [
                {
                    "script": {
                        "lang": "painless",
                        "source": """
        if (ctx.containsKey('pressure')) {
          ctx.pressure = ctx.pressure/1e5
        }
        if (ctx.containsKey('temperature')) {
          ctx.temperature = ctx.temperature - 273.15
        }
        """
                    }
                }
            ]
        }
    )
)

_SENSOR_DATA = {
    'location': {
        'lat': 59.972388858,
        'lon': 10.662971045
    },
    'alt': 607.827,
    'speed': 2.118,
    'timestamp': None,
    'humidity': None,
    'temperature': None,
    'pressure': None,
    'gps_valid': False,
    'acceleration': {
        'x': None,
        'y': None,
        'z': None
    }
}


def set_sensor_data():
    global _SENSOR_DATA
    for new_data in gps_socket:
        if new_data:
            data_stream.unpack(new_data)
            date_now = datetime.now(pytz.timezone(time.localtime().tm_zone))
            acc = sense.get_accelerometer_raw()
            sensor_data = {
                'location': {
                    'lat': data_stream.TPV['lat'] if type(data_stream.TPV['lat']) is float else 59.972388858,
                    'lon': data_stream.TPV['lon'] if type(data_stream.TPV['lon']) is float else 10.662971045,
                },
                'alt': data_stream.TPV['alt'] if type(data_stream.TPV['alt']) is float else 607.827,
                'speed': data_stream.TPV['speed'] if type(data_stream.TPV['speed']) is float else 2.118,
                'timestamp': date_now.isoformat(),
                'humidity': sense.get_humidity(),
                'temperature': sense.get_temperature() + 273.15,  # Convert to K
                'pressure': sense.get_pressure() * 1e+2,  # Convert from millibars to Pa
                'gps_valid': (
                    type(data_stream.TPV['lat']) is float and
                    type(data_stream.TPV['lon']) is float and
                    type(data_stream.TPV['alt']) is float and
                    type(data_stream.TPV['speed']) is float
                ),
                'acceleration': {
                    'x': acc['x'],
                    'y': acc['y'],
                    'z': acc['z'],
                    'abs': sqrt(acc['x']**2 + acc['y']**2 + acc['z']**2)
                }
            }

            _SENSOR_DATA = sensor_data
            es.index(
                index='{index_prefix}-{date}'.format(
                    index_prefix=index_prefix,
                    date=date_now.strftime('%Y-%m-%d')
                ),
                pipeline='convert_to_human_units',
                doc_type='_doc',
                body=json.dumps(sensor_data)
            )
            if debug:
                print(json.dumps(sensor_data, indent=2), flush=True)


def _valid_gps_data(sensor_data):

    return (
            type(sensor_data['location']['lat']) is float and
            type(sensor_data['location']['lon']) is float and
            type(sensor_data['alt']) is float and
            type(sensor_data['speed']) is float
    )


# Create and start thread for retrieving GPS data
th_gps = Thread(target=set_sensor_data)
th_gps.daemon = True
th_gps.start()


@app.route('/get_sensor_data', methods=['GET'])
def get_sensor_data():
    return jsonify(_SENSOR_DATA)


@app.route('/get_current_weather', methods=['GET'])
def get_current_weather():
    url_get_weather = 'https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={key}'.format(
        lat=_SENSOR_DATA['location']['lat'],
        lon=_SENSOR_DATA['location']['lon'],
        key=openweathermap_api_key
    )
    resp = requests.get(url_get_weather).json()

    return jsonify(resp)


@app.route('/get_weather_forecast', methods=['GET'])
def get_weather_forecast():
    url_get_weather = 'https://api.openweathermap.org/data/2.5/forecast?lat={lat}&lon={lon}&appid={key}'.format(
        lat=_SENSOR_DATA['location']['lat'],
        lon=_SENSOR_DATA['location']['lon'],
        key=openweathermap_api_key
    )
    resp = requests.get(url_get_weather).json()

    return jsonify(resp)


@app.route('/get_town_country', methods=['GET'])
def get_town_country():
    url_get_address = (
        'https://maps.googleapis.com/maps/api/geocode/json?latlng={lat},{lon}'.format(
            lat=_GPS_DATA['location']['lat'],
            lon=_GPS_DATA['location']['lon']
        ) +
        '&sensor=false&key={key}'.format(
            key=google_maps_api_key
        )
    )
    resp = requests.get(url_get_address).json()

    components = resp['results'][0]['address_components']
    country = town = None
    for c in components:
        if "country" in c['types']:
            country = c['long_name']
        if "postal_town" in c['types']:
            town = c['long_name']

    return jsonify({
        'town': town,
        'country': country
    })


if __name__ == '__main__':
    app.run(
        host='0.0.0.0',
        port=5678,
        debug=True
    )
