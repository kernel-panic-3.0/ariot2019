# Creating a VM on GCP with ES and Kibana

```bash
gcloud config set project arcitc-challenge-hackaton

gcloud config set compute/zone "europe-west1-b"

gcloud beta compute instances create elasticsearch-vm --machine-type=n1-standard-2 --subnet=default --network-tier=PREMIUM --maintenance-policy=MIGRATE --scopes=https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append --image=debian-9-stretch-v20180611 --image-project=debian-cloud --boot-disk-size=20GB --boot-disk-type=pd-ssd --boot-disk-device-name=elasticsearch-vm

gcloud compute ssh "elasticsearch-vm"
```

Install java 8:
```sh
sudo apt-get update
sudo apt-get install software-properties-common
sudo add-apt-repository "deb http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main"
sudo apt-get update
sudo apt-get install oracle-java8-installer
```

Setup Elasticsearch v 6.2.4 with learn-to-rank plugin:
```sh
wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-6.6.1.deb
sudo dpkg -i elasticsearch-6.6.1.deb
wget https://artifacts.elastic.co/downloads/kibana/kibana-6.6.1-amd64.deb
sudo dpkg -i kibana-6.6.1-amd64.deb
```

```sh
sudo vim /etc/sysctl.conf
# Add this line
vm.max_map_count=262144
```

Configure elasticsearch.yml
```
sudo vim /etc/elasticsearch/elasticsearch.yml
# Add these lines:
cluster.name: elasticsearch-v-6-2-4
node.name: elasticsearch-vm
path.data: /var/lib/elasticsearch
path.logs: /var/log/elasticsearch
network.host: 0.0.0.0
http.port: 9200
```

Start Elasticsearch as a service
```sh
sudo service elasticsearch start
#Look at things are ok
sudo service elasticsearch status
#Look at the logs
tail -f /var/log/elasticsearch/elasticsearch.log
```

Open the firewall make sure the VM can be accessed from the outside at ports 9200 an 5601 if desired.

## Start and Stop the TEST VM
It is important to stop the instance when not in use, so that we are not charged whenever the VM is not in use.

Start instance:
```sh
# Given that you have set the compute/zone property in your project
gcloud compute instances start "elasticsearch-vm"

# If you have not set up the compute/zone property, you have to specify it:
gcloud compute instances start "elasticsearch-vm" --zone="us-central1-f"
```

Stop the instance when done:
```sh
gcloud compute instances stop "elasticsearch-vm"
```

You need to make a ssh-tunnel to forward the connection from your localhost:9200 to the elasticsearch-host in port 9200.
```sh
gcloud compute --project "cx-smartsearch" ssh --zone "us-central1-f" "elasticsearch-vm" -- -L 9200:localhost:9200

# in the VM make sure elastisearch is running:
sudo service elasticsearch status
# and start it if it is not running:
sudo service elasticsearch start
```
